<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registro Actividades</title>
<!-- Links css -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous" />
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf"
	crossorigin="anonymous" />
<!-- Scripts de css -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
	integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
	integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
	crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-6">
				<div class="card border-dark mb-3">
					<h3>Registro Actividades</h3>
					<form action="newActividad" method="post">

						<div class="form-group">
							<div class="col-10">
								<label>Actividad</label> <input name="nombre"
									class="form-control" type="text" required="required" />
							</div>
							<div class="col-10">
								<label>Detalle</label> <input name="descripcion"
									class="form-control" type="text" required="required" />
							</div>
							<div class="col-10">
								<label>Fecha Finalizacion</label> <input name="fecha"
									type="date" class="form-control" required="required" />
							</div>
							<div class="col-10">
								<label>Asignada</label> <select required="required"
									name="usuario" class="custom-select custom-select-md" id="usu">
									<option>Seleccione un Usuario</option>
									<c:forEach items="${usu}" var="usu">
										<option value="${usu.id}">${usu.usuario}</option>
									</c:forEach>
								</select>
							</div>
							<br /> ${text} <br />
							<button class="btn btn-outline-info">Registrar</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-6">
				<br /> <br /> <a class="btn btn-info" href="readActividades">Consultar
					Tareas</a>
			</div>
		</div>
	</div>
</body>
</html>