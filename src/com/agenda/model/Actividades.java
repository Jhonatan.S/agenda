package com.agenda.model;
// Generated 12-12-2019 01:41:24 PM by Hibernate Tools 5.2.12.Final

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Actividades generated by hbm2java
 */
@Entity
@Table(name = "actividades", catalog = "agenda")
@SuppressWarnings("serial")
public class Actividades implements java.io.Serializable {

	private int id;
	private Estados estados;
	private Usuarios usuarios;
	private String nombre;
	private String descripcion;
	private Date fecha;
	private List<FasesDetalle> fasesDetalles = new ArrayList<FasesDetalle>();

	public Actividades() {
	}

	public Actividades(int id, Estados estados, Usuarios usuarios, String nombre, String descripcion, Date fecha) {
		this.id = id;
		this.estados = estados;
		this.usuarios = usuarios;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fecha = fecha;
	}

	public Actividades(int id, Estados estados, Usuarios usuarios, String nombre, String descripcion, Date fecha,
			List<FasesDetalle> fasesDetalles) {
		this.id = id;
		this.estados = estados;
		this.usuarios = usuarios;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fecha = fecha;
		this.fasesDetalles = fasesDetalles;
	}

	@Id

	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "estado", nullable = false)
	public Estados getEstados() {
		return this.estados;
	}

	public void setEstados(Estados estados) {
		this.estados = estados;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario", nullable = false)
	public Usuarios getUsuarios() {
		return this.usuarios;
	}

	public void setUsuarios(Usuarios usuarios) {
		this.usuarios = usuarios;
	}

	@Column(name = "nombre", nullable = false, length = 50)
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "descripcion", nullable = false, length = 65535)
	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "fecha", nullable = false, length = 10)
	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "actividades")
	public List<FasesDetalle> getFasesDetalles() {
		return this.fasesDetalles;
	}

	public void setFasesDetalles(List<FasesDetalle> fasesDetalles) {
		this.fasesDetalles = fasesDetalles;
	}

}
