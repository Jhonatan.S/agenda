package com.agenda.imp;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.agenda.model.Actividades;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import com.agenda.util.HibernateUtil;

@Repository
public class ActividadesImp extends AbstractFacade<Actividades> implements Dao<Actividades> {

	private Session session = HibernateUtil.getSessionFactory().openSession();

	public ActividadesImp() {
		super(Actividades.class);
	}

	/** Metodo para consultar actividades por id */
	public Actividades listXid(int d) {
		Actividades acti = new Actividades();
		List<Actividades> lis = new ArrayList<>();

		try {
			session.beginTransaction();

			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Actividades> cq = cb.createQuery(Actividades.class);
			Root<Actividades> act = cq.from(Actividades.class);
			cq.where(cb.equal(act.get("id"), d));

			lis = session.createQuery(cq).getResultList();
			acti = lis.get(0);
			session.getTransaction().commit();
			return acti;
		} catch (Exception e) {
			return null;
		}
	}
}
