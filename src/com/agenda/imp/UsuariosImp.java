package com.agenda.imp;

import java.util.List;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.agenda.model.Usuarios;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import com.agenda.util.HibernateUtil;

@Repository
public class UsuariosImp extends AbstractFacade<Usuarios> implements Dao<Usuarios> {

	private Session session = HibernateUtil.getSessionFactory().openSession();

	public UsuariosImp() {
		super(Usuarios.class);
	}

	// Metodo de inicio de session vefifica usuario y contraseņa y el estado
	@SuppressWarnings("unchecked")
	public Usuarios login(Usuarios user, String pass) {

		Transaction t = session.beginTransaction();
		try {
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Usuarios> cq = cb.createQuery(Usuarios.class);
			Root<Usuarios> root = cq.from(Usuarios.class);

			String query = "select * from usuarios where usuario=? and pass=?";
			Query q = session.createNativeQuery(query);
			q.setParameter(1, user.getUsuario());
			q.setParameter(2, pass);

			List<Usuarios> lista = q.getResultList();
			if (lista.size() > 0) {
				cq.where(cb.equal(root.get("usuario"), user.getUsuario()), cb.and(cb.equal(root.get("estado"), true)));
				user = session.createQuery(cq).uniqueResult();
			} else {
				user = null;
			}
		} catch (Exception e) {
			session.flush();
			return null;
		} finally {
			session.flush();
		}
		session.flush();
		t.commit();
		return user;

	}
}
