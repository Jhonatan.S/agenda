package com.agenda.imp;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.agenda.model.FasesDetalle;
import com.agenda.model.FechasEspeciales;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import com.agenda.util.HibernateUtil;

@Repository
@SuppressWarnings("unused")
public class FechasEspecialesImp extends AbstractFacade<FechasEspeciales> implements Dao<FechasEspeciales> {

	private Session session = HibernateUtil.getSessionFactory().openSession();

	public FechasEspecialesImp() {
		super(FechasEspeciales.class);
	}

}
