package com.agenda.imp;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.agenda.model.FasesDetalle;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import com.agenda.util.HibernateUtil;

@Repository
@SuppressWarnings("unused")
public class FasesDetalleImp extends AbstractFacade<FasesDetalle> implements Dao<FasesDetalle> {

	private Session session = HibernateUtil.getSessionFactory().openSession();

	public FasesDetalleImp() {
		super(FasesDetalle.class);
	}
}
