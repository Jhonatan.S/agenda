package com.agenda.imp;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.agenda.model.TiposUsuarios;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import com.agenda.util.HibernateUtil;

@Repository
@SuppressWarnings("unused")
public class TiposUsuariosImp extends AbstractFacade<TiposUsuarios> implements Dao<TiposUsuarios> {

	private Session session = HibernateUtil.getSessionFactory().openSession();

	public TiposUsuariosImp() {
		super(TiposUsuarios.class);
	}

}
