package com.agenda.imp;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.agenda.model.Estados;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import com.agenda.util.HibernateUtil;

@Repository
@SuppressWarnings("unused")
public class EstadosImp extends AbstractFacade<Estados> implements Dao<Estados> {

	private Session session = HibernateUtil.getSessionFactory().openSession();

	public EstadosImp() {
		super(Estados.class);
	}

}
