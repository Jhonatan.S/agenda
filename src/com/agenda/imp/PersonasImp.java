package com.agenda.imp;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.agenda.model.Personas;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import com.agenda.util.HibernateUtil;

@Repository
public class PersonasImp extends AbstractFacade<Personas> implements Dao<Personas> {

	private Session session = HibernateUtil.getSessionFactory().openSession();

	public PersonasImp() {
		super(Personas.class);
	}

	/** Metodo para consultar personas por id */
	public Personas listXid(int d) {
		Personas person = new Personas();
		List<Personas> lis = new ArrayList<>();

		try {
			session.beginTransaction();

			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Personas> cq = cb.createQuery(Personas.class);
			Root<Personas> per = cq.from(Personas.class);
			cq.where(cb.equal(per.get("id"), d));

			lis = session.createQuery(cq).getResultList();
			person = lis.get(0);
			session.getTransaction().commit();
			return person;
		} catch (Exception e) {
			return null;
		}
	}

}
