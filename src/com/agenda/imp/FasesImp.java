package com.agenda.imp;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.agenda.model.Fases;
import com.agenda.util.AbstractFacade;
import com.agenda.util.Dao;
import com.agenda.util.HibernateUtil;

@Repository
@SuppressWarnings("unused")
public class FasesImp extends AbstractFacade<Fases> implements Dao<Fases> {

	private Session session = HibernateUtil.getSessionFactory().openSession();

	public FasesImp() {
		super(Fases.class);
	}
}
