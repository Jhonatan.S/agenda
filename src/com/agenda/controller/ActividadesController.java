package com.agenda.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.agenda.imp.ActividadesImp;
import com.agenda.model.Actividades;
import com.agenda.model.Estados;
import com.agenda.model.Usuarios;
import com.agenda.util.Dao;

@Controller
@SuppressWarnings("unused")
public class ActividadesController {

	ModelAndView mav = new ModelAndView();

	@Autowired
	private Dao<Actividades> actividades;

	@Autowired
	private Dao<Usuarios> usuarios;

	@Autowired
	private Dao<Estados> estados;

	@Autowired
	private ActividadesImp actividad;

	/** Redirige a la vista de registro de una actividad */
	@RequestMapping(value = "/newActividad", method = RequestMethod.GET)
	public ModelAndView newActividad() {
		mav.addObject(new Actividades());

		List<Usuarios> usu = usuarios.findAll();
		List<Estados> est = estados.findAll();
		mav.addObject("usu", usu);
		mav.addObject("est", est);
		mav.setViewName("registroActividad");
		return mav;
	}

	/** Retorna la lista de actividades */
	@RequestMapping(value = "/readActividades", method = RequestMethod.GET)
	public ModelAndView readActividades() {
		List<Actividades> lista = new ArrayList<Actividades>();
		lista = actividades.findAll();
		System.out.println("este es el rango de la lista " + lista.size());
		mav.addObject("actividad", lista);
		mav.setViewName("actividades");
		return mav;
	}

	/** Registra una nueva actividad */
	@RequestMapping(value = "/newActividad", method = RequestMethod.POST)
	public ModelAndView createActividad(@RequestParam("nombre") String nombre,
			@RequestParam("descripcion") String descripcion, @RequestParam("fecha") String fecha,
			@RequestParam("usuario") int usuario) {

		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = dateFormat.parse(fecha);

			Estados e = new Estados();
			e.setId(1);

			Usuarios u = new Usuarios();
			u.setId(usuario);

			Actividades a = new Actividades();
			a.setNombre(nombre);
			a.setDescripcion(descripcion);
			a.setFecha(date);
			a.setEstados(e);
			a.setUsuarios(u);
			actividades.save(a);
			mav.addObject("text", "<b style='color: Green;'>Exito Actividad Registrada</b>");
		} catch (Exception e) {
			mav.addObject("text", "<b style='color: Green;'>Ha ocurrido un error inesperado</b>");
		}
		return newActividad();
	}

	/** Metodo para consultar por id de actividad */
	@RequestMapping(value = "/consultarActividad", method = RequestMethod.GET)
	public ModelAndView consultarXId(@RequestParam("cod") int id) {
		System.out.println("Llenando el numero " + id);
		Actividades lista = actividad.listXid(id);
		mav.addObject("lista", lista);
		mav.setViewName("editarActividad");
		return mav;
	}

	/** Metodo para actualizar actividades */
	@RequestMapping(value = "/updateActividad", method = RequestMethod.POST)
	public ModelAndView actualizarActividad(@RequestParam("nombre") String nombre,
			@RequestParam("descripcion") String descripcion, @RequestParam("fecha") String fecha,
			@RequestParam("usuario") int usuario) {

		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = dateFormat.parse(fecha);

			Estados e = new Estados();
			e.setId(1);

			Usuarios u = new Usuarios();
			u.setId(usuario);

			Actividades a = new Actividades();
			a.setNombre(nombre);
			a.setDescripcion(descripcion);
			a.setFecha(date);
			a.setEstados(e);
			a.setUsuarios(u);
			actividades.update(a);
			mav.addObject("msg", "<b style='color: Green;'>Exito Actividad Actualizada</b>");
		} catch (Exception e) {
			mav.addObject("msg", "<b style='color: Green;'>Ha ocurrido un error inesperado</b>");
		}
		return readActividades();
	}
}
