package com.agenda.util;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.agenda.imp.ActividadesImp;
import com.agenda.imp.EstadosImp;
import com.agenda.imp.FasesDetalleImp;
import com.agenda.imp.FasesImp;
import com.agenda.imp.FechasEspecialesImp;
import com.agenda.imp.PersonasImp;
import com.agenda.imp.TiposUsuariosImp;
import com.agenda.imp.UsuariosImp;
import com.agenda.model.Actividades;
import com.agenda.model.Estados;
import com.agenda.model.Fases;
import com.agenda.model.FasesDetalle;
import com.agenda.model.FechasEspeciales;
import com.agenda.model.Personas;
import com.agenda.model.TiposUsuarios;
import com.agenda.model.Usuarios;

@Configuration
@EnableWebMvc
@ComponentScan("com.agenda")
public class Config {

	@Bean
	InternalResourceViewResolver viewRes() {
		InternalResourceViewResolver r = new InternalResourceViewResolver();
		r.setPrefix("/WEB-INF/views/");
		r.setSuffix(".jsp");
		return r;
	}

	@Bean
	public SessionFactory getConex() {
		return HibernateUtil.getSessionFactory();
	}

	@Bean
	public Dao<Actividades> actividades() {
		return new ActividadesImp();
	}

	@Bean
	public ActividadesImp actividad() {
		return new ActividadesImp();
	}

	@Bean
	public Dao<Estados> estados() {
		return new EstadosImp();
	}

	@Bean
	public Dao<Fases> fases() {
		return new FasesImp();
	}

	@Bean
	public Dao<FasesDetalle> fasesDetalle() {
		return new FasesDetalleImp();
	}

	@Bean
	public Dao<FechasEspeciales> fechas() {
		return new FechasEspecialesImp();
	}

	@Bean
	public Dao<Personas> personas() {
		return new PersonasImp();
	}

	@Bean
	public Dao<TiposUsuarios> tiposUsuario() {
		return new TiposUsuariosImp();
	}

	@Bean
	public Dao<Usuarios> usuarios() {
		return new UsuariosImp();
	}

	@Bean
	public UsuariosImp usuario() {
		return new UsuariosImp();
	}

}
