package com.agenda.util;

import java.util.List;

public interface Dao<T> {

	public void save(T t);

	public void update(T t);

	public void delete(T t);

	List<T> findAll();
}
