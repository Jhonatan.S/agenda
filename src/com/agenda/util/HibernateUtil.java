package com.agenda.util;

import java.util.Properties;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import com.agenda.model.Actividades;
import com.agenda.model.Estados;
import com.agenda.model.Fases;
import com.agenda.model.FasesDetalle;
import com.agenda.model.FechasEspeciales;
import com.agenda.model.Personas;
import com.agenda.model.TiposUsuarios;
import com.agenda.model.Usuarios;

public class HibernateUtil {

	private static SessionFactory sessionFactory;

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			try {
				Configuration configuration = new Configuration();
				// Configuración de Hibernate equivalente a las propiedades hibernate.cfg.xml
				Properties settings = new Properties();
				settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
				settings.put(Environment.URL, "jdbc:mysql://localhost:3306/agenda?useSSL=false");
				settings.put(Environment.USER, "root");
				settings.put(Environment.PASS, "root");
				settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
				settings.put(Environment.SHOW_SQL, "true");
				settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
				//
				configuration.setProperties(settings);
				configuration.addAnnotatedClass(Actividades.class);
				configuration.addAnnotatedClass(Estados.class);
				configuration.addAnnotatedClass(Fases.class);
				configuration.addAnnotatedClass(FasesDetalle.class);
				configuration.addAnnotatedClass(FechasEspeciales.class);
				configuration.addAnnotatedClass(Personas.class);
				configuration.addAnnotatedClass(TiposUsuarios.class);
				configuration.addAnnotatedClass(Usuarios.class);
				ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
						.applySettings(configuration.getProperties()).build();
				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return sessionFactory;
	}
}
