create database agenda;
use agenda;

create table personas(
id int not null primary key auto_increment,
nombre varchar (50) not null,
apellido varchar(50) not null,
correo varchar(150) not null
);

 create table tipos_usuarios(
 id int not null primary key auto_increment,
 tipo varchar(20) not null
 );
 
 create table usuarios(
 id int not null primary key auto_increment,
 usuario varchar(50) not null unique,
 pass varchar(8) not null,
 tipo_usuario int not null,
 persona int not null,
 constraint fk1_tipo foreign key (tipo_usuario) references tipos_usuarios(id),
 constraint fk1_persona foreign key (persona) references personas(id)
 );
 
 create table estados(
 id int not null primary key auto_increment,
 estado varchar(20) not null
 );
 
 create table fases(
 id int not null primary key auto_increment,
 fase varchar (20) not null
 );
 
 create table actividades(
 id int not null primary key auto_increment,
 nombre varchar(50) not null,
 descripcion text not null,
 fecha date not null,
 estado int not null,
 usuario int not null,
 constraint fk_estado foreign key (estado) references estados(id),
 constraint fk_usuario foreign key (usuario) references usuarios(id)
 );
 
  create table fases_detalle(
 id int not null primary key auto_increment,
 actividad int not null,
 fecha_cambio date not null,
 fase int not null,
 constraint fk_actividad foreign key (actividad) references actividades(id),
 constraint fk_fase foreign key (fase) references fases(id)
 );
 
 create table fechas_especiales(
 id int primary key auto_increment not null,
 fecha date not null
 );