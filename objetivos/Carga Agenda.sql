use agenda;

insert into estados values
(1,'En espera'),
(2, 'Activo'),
(3, 'Finalizado');

insert into fases values
(1,'Activo'),
(2,'En Proceso'),
(3, 'Finalizado');

insert into tipos_usuarios values
(1, 'Administrador'),
(2, 'Gerente'),
(3, 'Asistente');

insert into personas values
(1, 'Jhonatan','Acevedo', 'jdj@gmail.com'),
(2, 'Tamara', 'Lopez', 'tami@gmail.com'),
(3, 'Militza', 'Diaz', 'md@gmail.com');

insert into usuarios values
(1, 'Jhonatan.S','12345678',1,1),
(2, 'Sofia', '12345678',2,2),
(3, 'Psicore', '12345678',3,3);